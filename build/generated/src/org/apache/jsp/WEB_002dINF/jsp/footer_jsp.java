package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class footer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link href=\"css/footer.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">-->\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");
      out.write("\n");
      out.write("  <!-- Site footer -->\n");
      out.write("        <footer>\n");
      out.write("            <div class=\"container-all\">\n");
      out.write("                <div class=\"container-body\">               \n");
      out.write("                    <div class=\"info\">\n");
      out.write("                        <h2>¿Quienes Somos?</h2>\n");
      out.write("                        <p class=\"text-justify\"> <i> MADERAS LISTAS E.I.R.L. </i> empresa dedicada a la importacion de maquinaria para trabajar la madera y la melamina, maquinas como: Escuadradoras, Enchapadoras de canto, CNC Router, Encoladora para canto curvos, Encoladora portátil, Garlopa aplanadora, entre otros. \"Somos una empresa especializada en la fabricacion de Closets, Reposteros, Mueblos en general que desde mas de 20 años brindamos calidad y confort a cada uno de nuestros clientes\".</p>                   \n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"redes\">\n");
      out.write("                        <h2>Redes Sociales</h2>\n");
      out.write("                        <a href=\"https://es-la.facebook.com/pages/category/Local-Business/Maderas-Listas-EIRL-194706363968888/\"><div class=\"row\">\n");
      out.write("                            <img width=\"40\" height=\"40\" src=\"image/facebook.png\">                       \n");
      out.write("                            <label>Siguenos en Facebook</label>                         \n");
      out.write("                        </div></a>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"contact\">\n");
      out.write("                        <h2>Informacion Contactos</h2>\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <img width=\"40\" height=\"40\" src=\"image/house.png\">                       \n");
      out.write("                            <label>Visítanos en nuestra oficina comercial : Av. Jesús 501-B, Mariano Melgar ó en nuestra planta de producción Urb. Ulrich Neisser B-7, Paucarpata (A una cuadra de la Region, Av. Kennedy cuadra 17, costado DEPROVE)</label>                         \n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <img width=\"40\" height=\"40\" src=\"image/phone.png\">                       \n");
      out.write("                            <label>Telefonos: (054) 464156</label>\n");
      out.write("                            <label>/ (054) 452991</label>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <img width=\"40\" height=\"40\" src=\"image/email.png\">                       \n");
      out.write("                            <label>maderaslistaseirl@hotmail.com</label>\n");
      out.write("                        </div>                    \n");
      out.write("                    </div>                                 \n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <div class=\"container-footer\">\n");
      out.write("                    <div class=\"copy\">\n");
      out.write("                        <a>Copyright &copy; 2020 Todos los Derechos Reservados por </a><a href=\"index.htm\">Maderas Listas E.I.R.L.</a>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"information\">\n");
      out.write("                        <a href=\"\">Informacion Compañia</a> | <a href=\"\">Privacidad y Politica</a> | <a href=\"\">Terminos y Condiciones</a>\n");
      out.write("                    </div>                   \n");
      out.write("            </div>\n");
      out.write("        </footer>\n");
      out.write("            ");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
