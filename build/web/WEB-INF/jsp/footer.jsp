<%-- 
    Document   : footer
    Created on : 16/01/2020, 02:42:24 PM
    Author     : Dell (Brucce Yul Villena Terreros)
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/footer.css" rel="stylesheet" type="text/css" />
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
    </head>
    <body>
        <%--<footer class="footer">
            <div class="container">
                <p>Pagina de Prueba v0.1 Footer @Maderas Listas 2020
            </div>
        </footer>--%>
  <!-- Site footer -->
        <footer>
            <div class="container-all">
                <div class="container-body">               
                    <div class="info">
                        <h2>¿Quienes Somos?</h2>
                        <p class="text-justify"> <i> MADERAS LISTAS E.I.R.L. </i> empresa dedicada a la importacion de maquinaria para trabajar la madera y la melamina, maquinas como: Escuadradoras, Enchapadoras de canto, CNC Router, Encoladora para canto curvos, Encoladora portátil, Garlopa aplanadora, entre otros. "Somos una empresa especializada en la fabricacion de Closets, Reposteros, Mueblos en general que desde mas de 20 años brindamos calidad y confort a cada uno de nuestros clientes".</p>                   
                    </div>

                    <div class="redes">
                        <h2>Redes Sociales</h2>
                        <a href="https://es-la.facebook.com/pages/category/Local-Business/Maderas-Listas-EIRL-194706363968888/"><div class="row">
                            <img width="40" height="40" src="image/facebook.png">                       
                            <label>Siguenos en Facebook</label>                         
                        </div></a>
                    </div>

                    <div class="contact">
                        <h2>Informacion Contactos</h2>
                        <div class="row">
                            <img width="40" height="40" src="image/house.png">                       
                            <label>Visítanos en nuestra oficina comercial : Av. Jesús 501-B, Mariano Melgar ó en nuestra planta de producción Urb. Ulrich Neisser B-7, Paucarpata (A una cuadra de la Region, Av. Kennedy cuadra 17, costado DEPROVE)</label>                         
                        </div>
                        <div class="row">
                            <img width="40" height="40" src="image/phone.png">                       
                            <label>Telefonos: (054) 464156</label>
                            <label>/ (054) 452991</label>
                        </div>
                        <div class="row">
                            <img width="40" height="40" src="image/email.png">                       
                            <label>maderaslistaseirl@hotmail.com</label>
                        </div>                    
                    </div>                                 
                </div>

            </div>
            <div class="container-footer">
                    <div class="copy">
                        <a>Copyright &copy; 2020 Todos los Derechos Reservados por </a><a href="index.htm">Maderas Listas E.I.R.L.</a>
                    </div>
                    <div class="information">
                        <a href="">Informacion Compañia</a> | <a href="">Privacidad y Politica</a> | <a href="">Terminos y Condiciones</a>
                    </div>                   
            </div>
        </footer>
            <%--<script src="js/footer.js"></script>--%>
    </body>
</html>