<%-- 
    Document   : header
    Created on : 16/01/2020, 02:42:24 PM
    Author     : Dell (Brucce Yul Villena Terreros)
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/header.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <header class="header">
          <div class="container logo-nav-container">
                <img class="logo_img" width="70" height="50" src="image/icono.jpg"><h1 class="logo"><a href="index.htm">Maderas Listas</a></h1>
                <span class="menu-icon" href=""><img width="50" height="50" src="https://image.flaticon.com/icons/svg/148/148795.svg"></span>
              <nav class="navegation">
                    <ul class="nav">
                      <li><a href="index.htm">Inicio</a></li>                      
                      <li><a href="category.html">Nosotros</a></li>                      
                      <li><a href="about.html">Colaboradores</a></li>                      
                      <li class="nav-li">
                        <a class="nav-li-a">Servicios</a>
                            <ul class="nav-li-a-ul">
                                <li><a class="nav-li-a-ul-li-a" href="">Productos</a></li>
                                <li><a class="nav-li-a-ul-li-a" href="">?????????</a></li>
                                <li><a class="nav-li-a-ul-li-a" href="">?????????</a></li>
                                <li><a class="nav-li-a-ul-li-a" href="">?????????</a></li>
                            </ul>
                      </li>
                      <li class="nav-li">
                        <a class="nav-li-a">Materiales</a>
                            <ul class="nav-li-a-ul">
                                <li><a class="nav-li-a-ul-li-a" href="">Submenu 5</a></li>
                                <li><a class="nav-li-a-ul-li-a" href="">Submenu 6</a></li>
                                <li><a class="nav-li-a-ul-li-a" href="">Submenu 7</a></li>
                                <li><a class="nav-li-a-ul-li-a" href="">Submenu 8</a></li>
                            </ul>
                      </li>
                    </ul>
              </nav>
          </div>
        </header>
        <script src="js/header.js"></script>
    </body>
</html> 