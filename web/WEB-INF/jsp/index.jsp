<%-- 
    Document   : index
    Created on : 16/01/2020, 02:42:24 PM
    Author     : Dell (Brucce Yul Villena Terreros)
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <title>Maderas Listas E.I.R.L.</title>

        <!-- Theme Style -->
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
      <!-- The header file start -->
      <header>
        <%@include file="header.jsp" %>
      </header>
      <!-- End the header file -->
        
      <!-- The main file start -->
      <main class="main">
          <div class="banner">
              <img src="image/banner 1.jpg" alt="" class="img_banner">
                  <div class="containers">
                    <h2 class ="titulo_banner">Piensa en un mejor titulo</h2> <br>
                    <p class="texto_banner">Con nostros la calidad esta asegurada</p>
                  </div>
          </div>
      </main>
      <!-- End the main file -->
      
      <!-- The footer file start -->
      <footer>
        <%@ include file="footer.jsp"%> 
      </footer>
      <!-- End the footer file -->
    </body>
</html>