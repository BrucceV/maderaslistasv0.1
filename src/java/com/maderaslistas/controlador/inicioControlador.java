package com.maderaslistas.controlador;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class inicioControlador {
    @RequestMapping ("inicio.htm")
    public ModelAndView inicio(){
        ModelAndView modelovista = new ModelAndView();
        modelovista.setViewName("inicio");
        return modelovista;
    }
    
}
